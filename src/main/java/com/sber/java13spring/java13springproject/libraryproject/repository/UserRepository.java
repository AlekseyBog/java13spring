package com.sber.java13spring.java13springproject.libraryproject.repository;

import com.sber.java13spring.java13springproject.libraryproject.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
      extends GenericRepository<User> {
}

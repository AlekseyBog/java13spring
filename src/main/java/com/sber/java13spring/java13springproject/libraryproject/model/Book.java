package com.sber.java13spring.java13springproject.libraryproject.model;

import com.fasterxml.jackson.annotation.*;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "books_seq", allocationSize = 1)
//@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@json_id")
public class Book
      extends GenericModel {
    
    @Column(name = "title", nullable = false)
    private String bookTitle;
    
    @Column(name = "publish_date", nullable = false)
    private LocalDate publishDate;
    
    @Column(name = "page_count")
    private Integer pageCount;
    
    @Column(name = "amount", nullable = false)
    private Integer amount;
    
    @Column(name = "storage_place", nullable = false)
    private String storagePlace;
    
    @Column(name = "online_copy_path")
    private String onlineCopyPath;
    
    @Column(name = "description")
    private String description;
    
    @Column(name = "genre", nullable = false)
    @Enumerated
    private Genre genre;
    
    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "books_authors",
               joinColumns = @JoinColumn(name = "book_id"), foreignKey = @ForeignKey(name = "FK_BOOKS_AUTHORS"),
               inverseJoinColumns = @JoinColumn(name = "author_id"), inverseForeignKey = @ForeignKey(name = "FK_AUTHORS_NOOKS"))
    //@JsonBackReference
    private Set<Author> authors;
    
    @OneToMany(mappedBy = "book")
    private Set<BookRentInfo> bookRentInfos;
}
